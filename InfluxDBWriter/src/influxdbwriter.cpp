/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QVariantHash>
#include <QVariantMap>
#include <QStringList>
#include <QString>
#include "influxdbwriter.h"

InfluxDBWriter::InfluxDBWriter(QObject *parent) : QObject(parent),
    mConnectionToDatabaseOk(false)
{
    connect(&mHeartbeat, &QTimer::timeout,
            this, &InfluxDBWriter::onTimeOut);

    connect(&mTxflushTimer, &QTimer::timeout,
            this, &InfluxDBWriter::onTxTimeout);

    connect(&mManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(onFinished(QNetworkReply*)),Qt::QueuedConnection);
}

void InfluxDBWriter::abortReguests()
{       
    foreach (request r,  mPendingReplies.values()) {
        r.httpReply->abort();
        r.httpReply->deleteLater();
    }
    mPendingReplies.clear();
}

InfluxDBWriter::~InfluxDBWriter()
{
    abortReguests();
}

bool InfluxDBWriter::usePostRequest(QUrl &reqAddr, QByteArray &payload,
                                    QString operationName, QString contentheaderType)
{
    bool ret = true;
    QNetworkRequest *req= new QNetworkRequest(reqAddr);

    req->setHeader(QNetworkRequest::ContentTypeHeader,QVariant(contentheaderType));
    QNetworkReply * reply = mManager.post(*req,payload);

    request r;
    r.httpReguest = req;
    r.httpReply = reply;
    r.operationName = operationName;

    mPendingReplies.insert(reply,r);

    return ret;
}

bool InfluxDBWriter::useGetRequest(QUrl &reqAddr,QUrlQuery query,
                                   QString operationName)
{
    bool ret = true;
    // https://stackoverflow.com/questions/15619153/setting-http-get-request-parameters-using-qt

    QUrl modified(reqAddr);
    modified.setQuery(query);
#if 0
    qDebug() << "tx: " << modified.toString();
#endif
    QNetworkRequest *req= new QNetworkRequest(modified);
    QNetworkReply * reply = mManager.get(*req);

    request r;
    r.httpReguest = req;
    r.httpReply = reply;
    r.operationName = operationName;

    req->setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    mPendingReplies.insert(reply,r);

    return ret;
}


void InfluxDBWriter::sendPing()
{
    mWaitingPingReply=true;
    QUrl reqAddr(mConnString+"/ping");

    QUrlQuery query;
    useGetRequest(reqAddr,query,"op.ping.remote");
}

void InfluxDBWriter::onRequestComplete(InfluxDBWriter::request r)
{
    /* Successful request handling here */
    if ( r.operationName=="op.ping.remote" ) {

        mWaitingPingReply=false;
    }
    else if (r.operationName=="op.db.create") {

        emit databaseCreatedAndTakenIntoUse(true);
    }
    else if (r.operationName=="op.db.clear") {

        emit databaseCleared(true);
    }
    else if (r.operationName=="op.db.dropMeasurement") {              
        emit removedMeasurement(true,QString(""));
    }
    else if (r.operationName=="op.db.writedata") {

        emit dataWritten();
        sendTxQueue();
    }
    else if (r.operationName=="op.db.showMeasure") {

        QString resp = r.httpReply->readAll().toStdString().data();
        QStringList r = parseMeasurementsJSON(resp);
        emit queryMeasureNamesResponse(true,r);
    }
    else if (r.operationName=="op.db.showTags") {

        QString resp = r.httpReply->readAll().toStdString().data();
        QHash<QString,QSet<QString> >   r = parseTagJSON(resp);
        emit queryAllTagsResponse(true,r);
    }
    else if (r.operationName=="op.db.showTagValues") {

        QString resp = r.httpReply->readAll().toStdString().data();
        QHash<QString, QHash<QString, QList<QString> > >  r = parseTagValuesJSON(resp);
        emit queryTagValuesResponse(true,r);
    }
    else if (r.operationName=="op.db.queryData") {

        QString resp = r.httpReply->readAll().toStdString().data();
        queryResultTag response;
        response = parseQueryDataJSON(resp);
        emit queryDataResponse(response);
    }
    else {
        qDebug() << __FUNCTION__ <<  ": unknown operation: " << r.operationName;
        Q_ASSERT(0);
    }
}

void InfluxDBWriter::onRequestFailed(InfluxDBWriter::request r,
                                     QNetworkReply::NetworkError error)
{
    Q_UNUSED(error);
    /* unsuccessful request handling here */
    qDebug() << "Request failed " << r.httpReply->errorString() << " " << r.operationName;

    /* UnSuccessful request handling here */
    if ( r.operationName=="op.ping.remote" ) {
        qDebug() << "error.database.ping.failed";
        emit disconnectedFromDB();
        abortReguests();
        mHeartbeat.stop();
        mConnString=""; // if no connstring, no tx possible
    }
    else if (r.operationName=="op.db.create") {
        qDebug() << "error.database.create.failed";
        mDBname="";
        emit databaseCreatedAndTakenIntoUse(false);
    }
    else if (r.operationName=="op.db.clear") {
        /* request failed */
        qDebug() << "error.database.clear.failed";
        emit databaseCleared(false);
    }
    else if (r.operationName=="op.db.dropMeasurement") {
        /* request failed */
        qDebug() << "op.db.dropMeasurement.failed";
        emit removedMeasurement(false,QString(""));
    }
    else if (r.operationName=="op.db.writedata") {
        qDebug() << "error.datawrite.failed";
    }
    else if (r.operationName=="op.db.showMeasure") {
        qDebug() << "error.showmeasureQuery.failed";
        InfluxDBWriter::queryResultTag result;
        result.queryWasSuccessful=false;
        emit queryDataResponse(result);
    }
    else if (r.operationName=="op.db.showTags") {
        qDebug() << "error.showTags.failed";
        emit queryTagValuesResponse(false, QHash<QString, QHash<QString, QList<QString> > >());
    }
    else if (r.operationName=="op.db.showTagValues") {
        qDebug() << "error.showTagValues.failed";
        emit queryAllTagsResponse(false,  QHash<QString,QSet<QString> >());
    }
    else if (r.operationName=="op.db.queryData") {
        qDebug() << "error.queryData.failed";
    }
    else {
        qDebug() << "unknown operation: " << r.operationName;
        Q_ASSERT(0);
    }
}

QString InfluxDBWriter::calculateTagSetAndMeasureName(const messageQueueItem &item)
{
    /* measureName format:   root1.key1.keyN.valueName
        * will resolve as tags: root1 key1 keyN with values root1=key1, key1=keyN, keyN=valueName
        * and value name valueName
        */

    // influx line protocol
    // cpu_load_short,direction=in,host=server01,region=us-west value=2.0

    QString ret;
    ret.append(item.measureName);

    for ( measureTag m : item.tags ) {
        ret.append(QString(",%1=%2").arg(m.tagName).arg(m.tagValue));
    }
    return ret;
}

void InfluxDBWriter::sendTxQueue()
{
    if (mLineProtocolMessageQueue.size()>0 ) {

        // https://docs.influxdata.com/influxdb/v1.2/guides/writing_data/
        // curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary
        // 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
        QUrl reqAddr(mConnString+QString("/write?db=%1&precision=ms").arg(mDBname));

        qint32 chunksize=0;

        QByteArray payload;

        const int maxMessagesPerHttpReq=10000;
        QString txString;
        while(chunksize<maxMessagesPerHttpReq && mLineProtocolMessageQueue.size()>0) {

            messageQueueItem item = mLineProtocolMessageQueue.takeFirst();

            QString valueFieldStr;
            for (queryValueField field : item.measureValue ) {

                QString numberStr = QString::number(field.fieldValue,'f',9);
                numberStr = numberStr.replace(",",".");

                valueFieldStr+=QString("%1=%2,").arg(field.fieldName).arg(numberStr);
            }
            valueFieldStr.chop(1);

            QString measureNameAndTags = calculateTagSetAndMeasureName(item);
            QString timestampStr = QString::number(item.timestamp.toMSecsSinceEpoch());

            txString.append(QString("%1 %2 %3\n").arg(measureNameAndTags)
                            .arg(valueFieldStr)
                            .arg(timestampStr));

            chunksize++;
        }
        payload =txString.toUtf8();
        usePostRequest(reqAddr,payload,"op.db.writedata","application/octet-stream");
    }

}

QStringList InfluxDBWriter::parseMeasurementsJSON(QString response)
{
    QStringList ret;

    /*
    {
    "results": [{
        "statement_id": 0,
        "series": [{
            "name": "measurements",
            "columns": ["name"],
            "values": [["rpm"],["rpm_without_tags"]]
        }]
    }]
    }
    */

    QByteArray t = response.toUtf8();

    QJsonParseError error;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(t,&error);
    if (jsonDoc.isNull()==false){
        if ( jsonDoc.isObject()==true) {
            QJsonObject obj = jsonDoc.object();

            QVariantMap varianMap = obj.toVariantMap();

            if ( varianMap.contains("results")==true) {

                QVariantList results = varianMap.value("results").toList();
                for ( QVariant result : results ) {

                    QVariantMap map = result.toMap();
                    if ( map.contains("series")==true) {

                        QVariantList series = map.value("series").toList();
                        for ( QVariant serie : series ) {

                            QVariantMap serieMap = serie.toMap();
                            if (serieMap.contains("values")==true) {

                                QVariantList values = serieMap.value("values").toList();
                                for (QVariant value : values) {

                                    QVariantList valueDatas = value.toList();
                                    for (QVariant valueData : valueDatas ){
                                        ret.append(valueData.toString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        qDebug() << error.errorString();
    }


    return ret;
}

QHash<QString,QSet<QString> >  InfluxDBWriter::parseTagJSON(QString response)
{
    QHash<QString,QSet<QString> >  ret;

    /*
    {
        "results": [{
            "statement_id": 0,
            "series": [{
                "name": "rpm",
                "columns": ["tagKey"],
                "values": [["car"],
                ["engine"]]
            },
            {
                "name": "rpm_without_tags",
                "columns": ["tagKey"],
                "values": [["car"],
                ["engine"]]
            }]
        }]
    }
    */

    QByteArray t = response.toUtf8();

    QJsonParseError error;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(t,&error);
    if (jsonDoc.isNull()==false){
        if ( jsonDoc.isObject()==true) {
            QJsonObject obj = jsonDoc.object();

            QVariantMap varianMap = obj.toVariantMap();

            if ( varianMap.contains("results")==true) {

                QVariantList results = varianMap.value("results").toList();
                for ( QVariant result : results ) {

                    QVariantMap map = result.toMap();
                    if ( map.contains("series")==true) {

                        QVariantList series = map.value("series").toList();
                        for ( QVariant serie : series ) {

                            QVariantMap serieMap = serie.toMap();

                            QString serieName ="";

                            if (serieMap.contains("name")==true) {
                                serieName = serieMap.value("name").toString();
                            }

                            if (serieMap.contains("values")==true) {

                                QVariantList values = serieMap.value("values").toList();
                                for (QVariant value : values) {

                                    QVariantList valueDatas = value.toList();
                                    for (QVariant valueData : valueDatas ){
                                        ret[serieName].insert(valueData.toString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        qDebug() << error.errorString();
    }
    return ret;
}

InfluxDBWriter::queryResultTag InfluxDBWriter::parseQueryDataJSON(QString response)
{
    /*

        "{
            \"results\": [
                {
                    \"series\": [
                        {
                            \"columns\": [
                                \"time\",
                                \"value\"
                            ],
                            \"name\": \"rpm\",
                            \"values\": [
                                [
                                    \"2017-07-03T17:05:16.338Z\",
                                    0
                                ],
                                [
                                    \"2017-07-03T17:05:21.239Z\",
                                    69
                                ]
                            ]
                        }
                    ],
                    \"statement_id\": 0
                }
            ]
        }
        "
    */

    queryResultTag ret;
    QByteArray t = response.toUtf8();

    QJsonParseError error;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(t,&error);
    if (jsonDoc.isNull()==false){
        if ( jsonDoc.isObject()==true) {
            QJsonObject obj = jsonDoc.object();

            QVariantMap varianMap = obj.toVariantMap();

            if ( varianMap.contains("results")==true) {

                QVariantList results = varianMap.value("results").toList();
                for ( QVariant result : results ) {

                    QVariantMap map = result.toMap();
                    if ( map.contains("series")==true) {

                        QVariantList series = map.value("series").toList();
                        for ( QVariant serie : series ) {

                            QVariantMap serieMap = serie.toMap();

                            QString serieName ="";

                            if (serieMap.contains("name")==true) {
                                serieName = serieMap.value("name").toString();
                            }
                            QStringList columnNames;
                            if (serieMap.contains("columns")==true) {
                                QVariantList colNames = serieMap.value("columns").toList();
                                for (QVariant colName : colNames ) {
                                    columnNames.append(colName.toString());
                                }
                            }

                            if (serieMap.contains("values")==true) {

                                QVariantList values = serieMap.value("values").toList();

                                queryDataPointTag valuePoint;
                                QDateTime timeStamp;
                                QHash<QString,qreal> valueList;
                                QSet<QString> tagList;

                                for (QVariant value : values ) {

                                    for ( int z=0;z<value.toList().size();z++) {
                                        QVariant v = value.toList().at(z);
                                        if (columnNames.at(z)=="time") {
                                            //2017-07-03T17:05:16.338Z
                                            QString dateStr = v.toString();
                                            timeStamp = QDateTime::fromString(dateStr,Qt::ISODateWithMs);
                                        }
                                        else if (columnNames.at(z).startsWith(measureFieldPrefix)==true) {
                                            QString n = columnNames.at(z);
                                            QString colName = n.replace(measureFieldPrefix,"");
                                            valueList.insert(colName,v.toDouble());
                                        }
                                        else {
                                            /* Skip. Is tag */
                                            QString n = columnNames.at(z);
                                            tagList.insert(n);
                                        }
                                    }

                                    valuePoint.timeStamp = timeStamp;
                                    valuePoint.valueFields = valueList;
                                    valuePoint.tagList = tagList;

                                    ret.measureName=serieName;
                                    ret.partialResult=false;
                                    ret.queryWasSuccessful=true;

                                    ret.results.insertMulti(timeStamp,valuePoint);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        qDebug() << error.errorString();
    }
    return ret;
}

QHash<QString,QHash<QString,QList<QString> > >
InfluxDBWriter::parseTagValuesJSON(QString response)
{
    /*
    {
        "results": [{
            "statement_id": 0,
            "series": [{
                "name": "rpm",
                "columns": ["key", "value"],
                "values": [["engine","0"],["engine","1"],["engine","2"],["engine","3"]]
            }]
        }]
    }
    */

    QHash<QString, QHash<QString, QList<QString> > >  ret;

    QByteArray t = response.toUtf8();

    QJsonParseError error;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(t,&error);
    if (jsonDoc.isNull()==false){
        if ( jsonDoc.isObject()==true) {
            QJsonObject obj = jsonDoc.object();

            QVariantMap varianMap = obj.toVariantMap();

            if ( varianMap.contains("results")==true) {

                QVariantList results = varianMap.value("results").toList();
                for ( QVariant result : results ) {

                    QVariantMap map = result.toMap();
                    if ( map.contains("series")==true) {

                        QVariantList series = map.value("series").toList();
                        for ( QVariant serie : series ) {

                            QVariantMap serieMap = serie.toMap();

                            QString serieName ="";

                            if (serieMap.contains("name")==true) {
                                serieName = serieMap.value("name").toString();
                            }

                            if (serieMap.contains("values")==true) {

                                QVariantList values = serieMap.value("values").toList();
                                for (QVariant value : values) {

                                    QString tagName;
                                    QString tagValue;

                                    for (int z=0;z<value.toList().size();z++) {

                                        if ( z==0) {
                                            tagName = value.toList().at(z).toString();
                                        }
                                        if (z==1) {
                                            tagValue = value.toList().at(z).toString();
                                        }
                                    }

                                    ret[serieName][tagName].append(tagValue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        qDebug() << error.errorString();
    }
    return ret;
}

bool InfluxDBWriter::connectTo(QString connstring)
{
    /*!
      \param connstring is address where you want connect to, for example: 'http://192.168.79.129:8086'.
    */

    bool ret=true;

    if ( mConnectionToDatabaseOk==true) {
        emit disconnectedFromDB();
        mConnectionToDatabaseOk=false;
        mHeartbeat.stop();
    }

    abortReguests();

    mConnString = connstring;

    sendPing();

    const int defaultHeartBeatPingIntervalMs=5000;
    mHeartbeat.start(defaultHeartBeatPingIntervalMs);

    return ret;
}

bool InfluxDBWriter::clearDB(QString dbName)
{
    /*!
      \param dbName is name of database you want clear (drop).
    */

    bool ret=true;

    QUrl reqAddr(mConnString+"/query");

    QUrlQuery qry;
    qry.addQueryItem("db",mDBname);
    reqAddr.setQuery(qry);

    QByteArray payload = QString("q=DROP DATABASE %1").arg(dbName).toUtf8();
    usePostRequest(reqAddr,payload,"op.db.clear");

    return ret;
}

bool InfluxDBWriter::createAndUseDB(QString dbName)
{
    bool ret=true;

    /*!
      \param dbName is name of database you want create (if not exists) and start using.
    */

    // https://docs.influxdata.com/influxdb/v1.2/guides/writing_data/
    QUrl reqAddr(mConnString+"/query");
    QUrlQuery qry;
    qry.addQueryItem("db",mDBname);
    reqAddr.setQuery(qry);


    QByteArray payload = QString("q=CREATE DATABASE %1").arg(dbName).toUtf8();
    usePostRequest(reqAddr,payload,"op.db.create");

    mDBname = dbName;

    return ret;
}

bool InfluxDBWriter::removeMeasurement(QString measurementName)
{
    bool ret=true;

    if ( mDBname.length()>0 && measurementName>0) {

        /*!
      \param measurementName is name of measure you want delete
    */

        qDebug() << "removing measurement " << measurementName;
        // https://docs.influxdata.com/influxdb/v1.2/query_language/spec/#delete
        QUrl reqAddr(mConnString+"/query");
        QUrlQuery qry;
        qry.addQueryItem("db",mDBname);
        reqAddr.setQuery(qry);

        QByteArray payload = QString("q=DROP MEASUREMENT \"%1\"").arg(measurementName).toUtf8();
        usePostRequest(reqAddr,payload,"op.db.dropMeasurement");
    }
    else {
        ret = false;
    }
    return ret;
}


void InfluxDBWriter::startTxTimer()
{
    if (mTxflushTimer.isActive()==false) {
        const int txFlushtimeMsec = 1000;
        mTxflushTimer.start(txFlushtimeMsec);
    }
}

bool InfluxDBWriter::writeMeasure(QString measureName,
                                  qreal measureValue,
                                  QList<InfluxDBWriter::measureTag> tags,
                                  QDateTime timestamp)
{
    bool ret=true;

    /*!
      \param measureName is name of measure using notation  "key1.key2.keyN-1.measurename"
      \param measureValue to be written.
      \param tags area measurevalue tags.
      \param timestamp is timeStamp. If omitted, current time is used.
    */

    if (mConnString.length()>0) {

        if ( timestamp.isValid()==false) {
            timestamp = QDateTime::currentDateTimeUtc();
        }

        messageQueueItem m;
        QString modifiedPath = measureName.simplified().replace(" ","_");
        m.measureName=modifiedPath;

        /* on single field case, we invent field name by ourselves */
        queryValueField field;
        field.fieldName=QString("%1%2").arg(measureFieldPrefix).arg("value1");
        field.fieldValue=measureValue;
        m.measureValue.append(field);

        m.timestamp=timestamp;
        m.tags = tags;

        mLineProtocolMessageQueue.append(m);

        startTxTimer();
        ret = true;
    }
    else {
        ret = false;
    }

    return ret;
}

bool InfluxDBWriter::writeLocationMeasure(QString measureName,
                                          qreal x, qreal y, qreal z,
                                          qreal measureValue,
                                          QList<InfluxDBWriter::measureTag> tags,
                                          QDateTime timestamp)
{
    bool ret=true;

    /*!
      \param measureName is name of measure using notation  "key1.key2.keyN-1.measurename"
      \param x is position x coordinate
      \param y is position y coordinate
      \param z is position z coordinate
      \param measureValue to be written.
      \param tags area measurevalue tags.
      \param timestamp is timeStamp. If omitted, current time is used.
    */

    if (mConnString.length()>0) {

        if ( timestamp.isValid()==false) {
            timestamp = QDateTime::currentDateTimeUtc();
        }

        messageQueueItem m;
        QString modifiedPath = measureName.simplified().replace(" ","_");
        m.measureName=modifiedPath;

        /* On single field case, we invent field name by ourselves */
        /* Add alphabetical order for better Influx performance */
        queryValueField field;
        field.fieldName=QString("%1%2").arg(measureFieldPrefix).arg("x");
        field.fieldValue=x;
        m.measureValue.append(field);

        field.fieldName=QString("%1%2").arg(measureFieldPrefix).arg("valueAtpos");
        field.fieldValue=measureValue;
        m.measureValue.append(field);

        field.fieldName=QString("%1%2").arg(measureFieldPrefix).arg("y");
        field.fieldValue=y;
        m.measureValue.append(field);

        field.fieldName=QString("%1%2").arg(measureFieldPrefix).arg("z");
        field.fieldValue=z;
        m.measureValue.append(field);

        m.timestamp=timestamp;
        m.tags = tags;

        mLineProtocolMessageQueue.append(m);

        startTxTimer();
        ret = true;
    }
    else {
        ret = false;
    }

    return ret;
}

bool InfluxDBWriter::queryAllTags() {

    bool ret=true;
    QUrl reqAddr(mConnString+"/query");

    QUrlQuery query;
    query.addQueryItem("q",QString("SHOW TAG KEYS ON %1").arg(mDBname));
    query.addQueryItem("db",mDBname);

    // for Select and Show, GET needs to be used
    useGetRequest(reqAddr,query,"op.db.showTags");

    return ret;
}

bool InfluxDBWriter::queryAllTagValues(QString tagName)
{
    bool ret=true;
    QUrl reqAddr(mConnString+"/query");

    QUrlQuery query;
    query.addQueryItem("q",QString("SHOW TAG VALUES ON %2 WITH KEY = %1")
                       .arg(tagName)
                       .arg(mDBname));

    query.addQueryItem("db",mDBname);

    // for Select and Show, GET needs to be used
    useGetRequest(reqAddr,query,"op.db.showTagValues");

    return ret;
}

bool InfluxDBWriter::queryMeasureData(QString measureName, QList<measureTag> filterTags,
                                      QDateTime startTime, QDateTime finishTime)
{
    bool ret = true;

    qDebug() << __FUNCTION__ << " for " << measureName;

    // Perform query and ask results streamed in batches
    QUrl reqAddr(mConnString+"/query");

    QUrlQuery query;
    query.addQueryItem("db",mDBname);


    /* url -G 'http://localhost:8086/query?pretty=true' --data-urlencode "db=mydb"
     * --data-urlencode "q=SELECT \"value\"
     * FROM \"cpu_load_short\" WHERE \"region\"='us-west'" */


    // where time > '2013-08-12 23:32:01.232' and time < '2013-08-13';

    // chunking
#if 0
    // does not work yet
    query.addQueryItem("chunked","true");
    query.addQueryItem("chunk_size","4");
#endif
    if (filterTags.size()==0) {
        query.addQueryItem("q",QString("SELECT * FROM %1 WHERE time > '%2' AND time <'%3'")
                           .arg(measureName)
                           .arg(startTime.toUTC().toString(Qt::ISODateWithMs))
                           .arg(finishTime.toUTC().toString(Qt::ISODateWithMs)));
    }
    else {
        QString tagFilterStr;
        for (measureTag m : filterTags ) {
            QString tag = QString("\"%1\"='%2'").arg(m.tagName).arg(m.tagValue);
            tagFilterStr+=" AND ";
            tagFilterStr+=tag;
        }

        query.addQueryItem("q",QString("SELECT * FROM %1 WHERE time > '%2' AND time <'%3'%4")
                           .arg(measureName)
                           .arg(startTime.toUTC().toString(Qt::ISODateWithMs))
                           .arg(finishTime.toUTC().toString(Qt::ISODateWithMs))
                           .arg(tagFilterStr));
    }
#if 1
    query.addQueryItem("pretty","true");
#endif

    // for Select and Show, GET needs to be used
    useGetRequest(reqAddr,query,"op.db.queryData");

    return ret;
}

quint16 InfluxDBWriter::getPendingOperationCount()
{
    return mPendingReplies.size();
}

bool InfluxDBWriter::queryMeasureNames() {

    bool ret=true;
    QUrl reqAddr(mConnString+"/query");

    QUrlQuery query;
    query.addQueryItem("q",QString("SHOW MEASUREMENTS"));

    query.addQueryItem("db",mDBname);

    // for Select and Show, GET needs to be used
    useGetRequest(reqAddr,query,"op.db.showMeasure");

    return ret;
}

void InfluxDBWriter::onTimeOut()
{
    if ( mWaitingPingReply==true ) {
        emit disconnectedFromDB();
        mConnectionToDatabaseOk=false;
        mHeartbeat.stop();
    }
    else {
        if (mConnectionToDatabaseOk==false) {
            emit connectedToDB();
        }
        mConnectionToDatabaseOk=true;
        sendPing();
    }
}

void InfluxDBWriter::onTxTimeout()
{
    sendTxQueue();

    if ( mLineProtocolMessageQueue.size()==0) {
        mTxflushTimer.stop();
    }
}

void InfluxDBWriter::cleanUpRequest(request r, QNetworkReply *reply)
{
    delete r.httpReguest;
    r.httpReply->deleteLater();
    r.httpReply=NULL;
    r.httpReguest=NULL;
    mPendingReplies.remove(reply);
}

void InfluxDBWriter::onFinished(QNetworkReply *reply)
{

    if (mPendingReplies.contains(reply)==true) {

        request r = mPendingReplies.value(reply);

        /* Handle response */
        if ( reply->isFinished()==true) {
            QNetworkReply::NetworkError error = reply->error();
            if (error==QNetworkReply::NoError) {
                /* response to request was ok */
                onRequestComplete(r);
            }
            else {
                onRequestFailed(r,error);
            }
        }
        cleanUpRequest(r, reply);
    }
    else {
        qDebug() << "unknown reply " << reply->url().toString();
    }
}

