/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HEATMAP_H
#define HEATMAP_H

#include <QQuickPaintedItem>
#include <QObject>
#include <QPainter>
#include <QGeoShape>
#include <QGeoRectangle>
#include <QDebug>
#include <QVector>

class HeatMap : public QQuickPaintedItem
{
    Q_OBJECT
public:
    explicit HeatMap();

    void paint(QPainter *painter);

    /*! Call this after visible area change or before adding data.
     * This is will adjust heatmap size and reset counters */
    void initHeatMap();

    /* Add incrementially metric value to cell */
    void incrementMetricData(qreal latitude, qreal longitude,qreal increment);

    void setMetricData(qreal latitude, qreal longitude,qreal newValue);

    void resetMetricData();

    Q_INVOKABLE void registerHeatMap(QString heatMapName);

signals:

public slots:
    void onVisibleAreaChanged(QGeoShape newArea);
private:

    struct heatMapInfoTag {
        QGeoRectangle currentGeoRect;
        quint16 columns;
        quint16 rows;
        qreal offsetX;
        qreal offsetY;
    } mHeatMapInfo;

    struct heatMapItemTag {
        QGeoRectangle box;
        QRectF drawBox;
        qreal calculationValueSum;
        quint32 calculationValueItemsCount;
    };

    QList<heatMapItemTag> mHeatMapItems;

    QGeoRectangle mVisibleGeoRect;

    int getCellIndex(qreal latitude, qreal longitude);
};

#endif // HEATMAP_H
