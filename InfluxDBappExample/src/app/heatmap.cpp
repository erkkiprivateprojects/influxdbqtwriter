/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "heatmap.h"
#include <QGeoRectangle>

#include "dbappexample.h"
#include "timetopiclogger.h"

HeatMap::HeatMap(){      
}

void HeatMap::paint(QPainter *painter)
{
    TTPLOG_FUNCTIONSCOPE;



    for (heatMapItemTag item : mHeatMapItems) {
        if ( item.calculationValueSum>0) {

            QBrush myBrush;
            QColor myColor("green");
            myColor.setAlphaF(0.2);
            myBrush.setColor(myColor);
            myBrush.setStyle(Qt::SolidPattern);

            painter->setBrush(myBrush);
            QPen p;
            p.setWidth(item.drawBox.width()/32.0);
            p.setColor(Qt::transparent);
            painter->setPen(p);
            painter->drawRect(item.drawBox);
            QFont myFont("Courier");
            myFont.setPixelSize(item.drawBox.height()*3/4.0);
            painter->setFont(myFont);
            painter->setPen(Qt::black);
            painter->drawText(item.drawBox,QString("%2").arg(item.calculationValueSum),QTextOption(Qt::AlignHCenter |Qt::AlignVCenter));
        }
        else {
            painter->setBrush(Qt::transparent);
        }

    }
}

void HeatMap::initHeatMap()
{
    TTPLOG_FUNCTIONSCOPE;
    mHeatMapItems.clear();

    if ( mVisibleGeoRect.isValid()==true) {

        qreal gridEddeSizeMeters = 100.0;

        qreal widthInM = mVisibleGeoRect.topLeft().distanceTo(mVisibleGeoRect.topRight());
        qreal widthDiv = widthInM/gridEddeSizeMeters;

        qreal heightInM = mVisibleGeoRect.topLeft().distanceTo(mVisibleGeoRect.bottomLeft());
        qreal heightDiv = heightInM/gridEddeSizeMeters;

        const int maxDiv = 128;
        if (widthDiv>maxDiv) {
            widthDiv = maxDiv;
        }

        if (heightDiv>maxDiv) {
            heightDiv = maxDiv;
        }

        qreal w = mVisibleGeoRect.width()/widthDiv;
        qreal h = mVisibleGeoRect.height()/heightDiv;

        if ( w>0) {

            int longStart = mVisibleGeoRect.topLeft().longitude();
            int latStart = mVisibleGeoRect.topLeft().latitude();

            qreal fX = 0;
            while ((qreal)longStart+fX* w<mVisibleGeoRect.topLeft().longitude()) {
                fX+=1.0;
            }
            if ( fX>0) {
                fX-=1.0;
            }

            qreal fY = 0;
            while ((qreal)latStart+fY*h<mVisibleGeoRect.topLeft().latitude()) {
                fY+=1.0;
            }

            qreal offX = mVisibleGeoRect.topLeft().longitude()-fX*w-(qreal)longStart;
            qreal offY = mVisibleGeoRect.topLeft().latitude()-fY*h-(qreal)latStart;

            QRect box(0,0,this->boundingRect().width()/widthDiv,this->boundingRect().height()/heightDiv);

            mHeatMapInfo.columns=0;
            mHeatMapInfo.rows=0;
            mHeatMapInfo.currentGeoRect=mVisibleGeoRect;
            mHeatMapInfo.offsetX = offX;
            mHeatMapInfo.offsetY = offY;

            int row = 0;
            int col = 0;

            for ( qreal longitude= mVisibleGeoRect.topLeft().longitude();longitude<mVisibleGeoRect.topRight().longitude();longitude+=w) {
                for ( qreal latitude= mVisibleGeoRect.topLeft().latitude();latitude>mVisibleGeoRect.bottomLeft().latitude();latitude-=h) {

                    qreal longitudeOffset = longitude-mVisibleGeoRect.topLeft().longitude();
                    qreal latitudeOffset = mVisibleGeoRect.topLeft().latitude()-latitude;

                    int x = (longitudeOffset/(qreal)mVisibleGeoRect.width()*(qreal)this->boundingRect().width());
                    x-=(offX/(qreal)mVisibleGeoRect.width())*(qreal)this->boundingRect().width();

                    int y = (latitudeOffset/(qreal)mVisibleGeoRect.height()*(qreal)this->boundingRect().height());
                    y+=(offY/(qreal)mVisibleGeoRect.height())*(qreal)this->boundingRect().height();

                    box.moveTopLeft(QPoint(x,y));

                    heatMapItemTag heatMapItem;
                    heatMapItem.box = QGeoRectangle(QGeoCoordinate(latitude-offY,longitude-offX),
                                                    QGeoCoordinate(latitude-offY-h,longitude-offX+w));

                    heatMapItem.drawBox = box;
                    heatMapItem.calculationValueItemsCount=0;
                    heatMapItem.calculationValueSum=0;
                    mHeatMapItems.append(heatMapItem);
                    row++;
                }
                mHeatMapInfo.rows=row;
                row=0;
                col++;
            }
            mHeatMapInfo.columns=col;
            col=0;
        }
    }
}

int HeatMap::getCellIndex(qreal latitude, qreal longitude)
{
    // check if item hits to current map. Create compare rect. Make it small enough
    const qreal divratio = 1.0/16.0;
    QGeoRectangle compare(QGeoCoordinate(latitude,longitude),
                          mHeatMapInfo.currentGeoRect.width()*(1.0/(qreal)mHeatMapInfo.columns)*divratio,
                          mHeatMapInfo.currentGeoRect.height()*(1.0/(qreal)mHeatMapInfo.rows)*divratio
                          );

    compare.setCenter(QGeoCoordinate(latitude,longitude));

    int index =-1;
    if ( mHeatMapInfo.currentGeoRect.intersects(compare)==true) {

        /* Calculate rough location */

        int col = ((longitude+mHeatMapInfo.offsetX-mHeatMapInfo.currentGeoRect.topLeft().longitude())/
                   mHeatMapInfo.currentGeoRect.width()+mHeatMapInfo.offsetX)*
                mHeatMapInfo.columns;

        int row = ((mHeatMapInfo.currentGeoRect.topLeft().latitude()-mHeatMapInfo.offsetY-latitude)/
                   mHeatMapInfo.currentGeoRect.height())*
                mHeatMapInfo.rows;

        /* Test if found nearby rough location */
        int candidateCell = -1;
        for ( int x=col-1;x<col+2;x++) {
            for ( int y=row-1;y<row+2;y++) {
                candidateCell = x * mHeatMapInfo.rows + y;
                if ( candidateCell >=0 && candidateCell <mHeatMapItems.size() ) {
                    if(mHeatMapItems.at(candidateCell).box.intersects(compare)==true) {
                        index = candidateCell;
                        break;
                    }
                }
            }
        }
    }

    return index;
}

void HeatMap::incrementMetricData(qreal latitude, qreal longitude, qreal increment) {

    TTPLOG_FUNCTIONSCOPE;

    int index = getCellIndex(latitude, longitude);

    if ( index>=0 && index < mHeatMapItems.size()) {
        mHeatMapItems[index].calculationValueSum+=increment;
        mHeatMapItems[index].calculationValueItemsCount++;
    }

    update();
}

void HeatMap::setMetricData(qreal latitude, qreal longitude, qreal newValue)
{
    TTPLOG_FUNCTIONSCOPE;

    int index = getCellIndex(latitude, longitude);

    if ( index>=0 && mHeatMapItems.size()) {
        mHeatMapItems[index].calculationValueSum=newValue;
        mHeatMapItems[index].calculationValueItemsCount++;
    }
}

void HeatMap::resetMetricData()
{
    for (int k=0;k<mHeatMapItems.size();k++) {
        mHeatMapItems[k].calculationValueItemsCount=0;
        mHeatMapItems[k].calculationValueSum=0;
    }

    update();
}

void HeatMap::registerHeatMap(QString heatMapName)
{
    DBAppExample::instance()->registerHeatMap(heatMapName,this);
}

void HeatMap::onVisibleAreaChanged(QGeoShape newArea)
{
    mVisibleGeoRect = newArea.boundingGeoRectangle();

    initHeatMap();
}
