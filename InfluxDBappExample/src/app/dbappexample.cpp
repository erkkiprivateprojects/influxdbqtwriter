/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <QDebug>
#include "dbappexample.h"
#include "timetopiclogger.h"

static DBAppExample *gApp = NULL;

DBAppExample::DBAppExample(QObject *parent) : QObject(parent),
    mdbWriter(NULL),mDatabaseName("influxDBappExample"),mCurrentMeasureIndex(0)
{
    if (gApp==NULL ) {
        gApp = this;
    }

    const int startUpTimeOutMs = 100;
    QTimer::singleShot(startUpTimeOutMs,this,&DBAppExample::start);

    TTPLOG_POSTCODE("START");
}

DBAppExample::~DBAppExample()
{
    mdbWriter->deleteLater();
    mdbWriter=NULL;
}

DBAppExample *DBAppExample::instance()
{
    return gApp;
}

void DBAppExample::registerHeatMap(QString heatMapName, HeatMap *instanceRef)
{
    mHeatMaps[heatMapName] = instanceRef;
}

void DBAppExample::unRegisterHeatMap(QString heatMapName)
{
    mHeatMaps.remove(heatMapName);
}

void DBAppExample::start()
{
    TTPLOG_FUNCTIONSCOPE;

    mdbWriter = new InfluxDBWriter();

    connect(mdbWriter, &InfluxDBWriter::connectedToDB,
            this, &DBAppExample::onConnectedToDB);

    connect(mdbWriter, &InfluxDBWriter::disconnectedFromDB,
            this, &DBAppExample::onDisconnectedFromDB);

    connect(mdbWriter, &InfluxDBWriter::databaseCleared,
            this, &DBAppExample::onDatabaseCleared);

    connect(mdbWriter, &InfluxDBWriter::databaseCreatedAndTakenIntoUse,
            this, &DBAppExample::onDatabaseCreatedAndTakenIntoUse);

    connect(mdbWriter, &InfluxDBWriter::dataWritten,
            this, &DBAppExample::onDataWritten);

    connect(mdbWriter, &InfluxDBWriter::queryAllTagsResponse,
            this, &DBAppExample::onQueryAllTagsResponse);

    connect(mdbWriter, &InfluxDBWriter::queryMeasureNamesResponse,
            this, &DBAppExample::onQueryMeasureNamesResponse);

    connect(mdbWriter, &InfluxDBWriter::queryDataResponse,
            this, &DBAppExample::onQueryDataResponse);

    connect(mdbWriter, &InfluxDBWriter::queryTagValuesResponse,
            this, &DBAppExample::onQueryTagValuesResponse);

    connect(&mDataRefreshTimer, &QTimer::timeout,
            this, &DBAppExample::onDataRefreshTimeOut);

    // your influx db address
    mdbWriter->connectTo(connectionString);
    emit connectingToDataBase(connectionString);

    TTPLOG_POSTCODE(QString("Connecting to "+connectionString));

    mLastUpdate.start();
}

void DBAppExample::onAddNewPosition(QGeoCoordinate newPosition)
{
    TTPLOG_FUNCTIONSCOPE;

    // test location measure API
    const int minPosititionRefreshIntervalMs = 100;
    if ( mLastUpdate.elapsed()>minPosititionRefreshIntervalMs) {
        mLastUpdate.start();

        QList<InfluxDBWriter::measureTag> locationTags;
        InfluxDBWriter::measureTag locTag1;
        locTag1.tagName="area";
        locTag1.tagValue="Oslo";
        locationTags.append(locTag1);

        mdbWriter->writeLocationMeasure(getCurrentMeasurementName(),
                                        newPosition.longitude(),
                                        newPosition.latitude(),
                                        newPosition.altitude(),
                                        1,
                                        locationTags);

        const int defaultRefreshTimeOutMs=500;
        if  (mDataRefreshTimer.isActive()==false) {
            mDataRefreshTimer.start(defaultRefreshTimeOutMs);
        }
    }
}

void DBAppExample::onClearData()
{
    mdbWriter->clearDB(mDatabaseName);
    mColorTable.clear();
}

void DBAppExample::onDisplayUpdateRequest()
{    
    TTPLOG_FUNCTIONSCOPE;

    qDebug() << "a";
    if ( mdbWriter!=NULL) {
        qDebug() << "b" <<mdbWriter->getPendingOperationCount();
        if ( mdbWriter->getPendingOperationCount()<=1) {
            mdbWriter->queryMeasureNames();
        }
    }
}

void DBAppExample::onStartNewSerie()
{
    /* When mouse is pressed, new measurement serie is started.
     * This is will be used for demonstrating use of labels */
    updateNextMeasurementName();
    /* Clear possible previous data */
    mdbWriter->removeMeasurement(getCurrentMeasurementName());
}

void DBAppExample::addMeasureFilterItem(QString itemToBeFilteredOut)
{
        mMeasureFilters.insert(itemToBeFilteredOut);
}

void DBAppExample::removeMeasureFilterItem(QString itemToBeRemovedOut)
{
    mMeasureFilters.remove(itemToBeRemovedOut);
}

void DBAppExample::clearMeasureFilter() {
    mMeasureFilters.clear();
}

void DBAppExample::onConnectedToDB()
{
    TTPLOG_FUNCTIONSCOPE;

    TTPLOG_POSTCODE(QString("Connected to "+connectionString));

    emit connectedToDatabase();
    if (mdbWriter->createAndUseDB(mDatabaseName)==true) {
    }
}

void DBAppExample::onDisconnectedFromDB()
{
    TTPLOG_FUNCTIONSCOPE;

    mDataRefreshTimer.stop();
    emit disconnectedFromDataBase();
}

void DBAppExample::onDatabaseCleared(bool wasSuccess)
{
    TTPLOG_FUNCTIONSCOPE;

    if (wasSuccess==true) {
        /* Writing to DB can now start */
        if (mdbWriter->createAndUseDB(mDatabaseName)==true) {
        }
    }
}

void DBAppExample::onDatabaseCreatedAndTakenIntoUse(bool operationOk)
{
    TTPLOG_FUNCTIONSCOPE;

    if ( operationOk==true) {
        /* Writing to DB can now start */
        qDebug() << "ready for logging";
        TTPLOG_POSTCODE(QString("DB READY"));
        /* query initial data */
        onDisplayUpdateRequest();
    }
}

void DBAppExample::onDataWritten()
{
    qDebug() << "database updated";
}

void DBAppExample::onQueryMeasureNamesResponse(bool wasSuccess,
                                               QStringList measureNames)
{
    TTPLOG_FUNCTIONSCOPE;

    if ( wasSuccess == true ) {

        qDebug() << __FUNCTION__ << "measure names available " <<  measureNames;

        QVariantList meas;
        QVariantList measColors;
        for (QString measure : measureNames ) {
            meas.append(measure);

            createColorForMeasurement(measure);
            measColors.append(getMeasurementColor(measure));
        }

        emit measureNamesUpdated(meas,measColors);

        if ( mHeatMaps.contains("exampleMap")==true) {
            mHeatMaps["exampleMap"]->resetMetricData();
        }

        for (QString measure : measureNames ) {

            if ( mMeasureFilters.contains(measure)==false ) {

                QList<InfluxDBWriter::measureTag> sample;
                const int secsIntoPast = 3600;

                TTPLOG_EVENT(QString("qry_%1").arg(measure),true);

                mdbWriter->queryMeasureData(measure,
                                            sample,
                                            QDateTime::currentDateTime().addSecs(-1*secsIntoPast),
                                            QDateTime::currentDateTime());
            }
        }
    }
}

void DBAppExample::onQueryAllTagsResponse(bool wasSuccess,
                                          QHash<QString, QSet<QString> > tagNames)
{
    /* Currently not in use */
    Q_UNUSED(wasSuccess);
    Q_UNUSED(tagNames);
}

void DBAppExample::onQueryTagValuesResponse(bool wasSuccess,
                                            QHash<QString, QHash<QString,
                                            QList<QString> > > tagValues)
{
    /* Currently not in use */
    Q_UNUSED(wasSuccess);
    Q_UNUSED(tagValues);
}

void DBAppExample::onQueryDataResponse(InfluxDBWriter::queryResultTag result)
{

    TTPLOG_FUNCTIONSCOPE;

    if ( result.queryWasSuccessful==true) {
        //if (result.partialResult==true) {
        if ( 1 ) {
            /* collect */
            qDebug() << __FUNCTION__ << "data available " <<  result.measureName << result.results.size() << " datapoints";

            TTPLOG_EVENT(QString("qry_%1").arg(result.measureName),false);

            createColorForMeasurement(result.measureName);

            if ( mMeasureFilters.contains(result.measureName)==false ) {
                for (const InfluxDBWriter::queryDataPointTag v : result.results) {

                    QGeoCoordinate pathPoint;
                    pathPoint.setLongitude(v.valueFields["x"]);
                    pathPoint.setLatitude(v.valueFields["y"]);
                    pathPoint.setAltitude(v.valueFields["z"]);

                    emit newPathPoint(pathPoint,getMeasurementColor(result.measureName));

                    if ( mHeatMaps.contains("exampleMap")==true) {
                        mHeatMaps["exampleMap"]->incrementMetricData(pathPoint.latitude(),
                                                                     pathPoint.longitude(),
                                                                     1.0);

                    }
                }
            }

        }
        else {
            /* last */
            qDebug() << __FUNCTION__ << "data available " <<  result.results.size() << " datapoints";
        }
    }
}

void DBAppExample::onDataRefreshTimeOut()
{
    onDisplayUpdateRequest();
    mDataRefreshTimer.stop();
}

void DBAppExample::updateNextMeasurementName() {
    mCurrentMeasureIndex++;
    const quint8 maxMeasurementsSeries = 8;
    if ( mCurrentMeasureIndex>maxMeasurementsSeries ) {
        mCurrentMeasureIndex=0;
    }
}

QString DBAppExample::getCurrentMeasurementName() {
    return QString("pointSerie%1").arg(mCurrentMeasureIndex);
}

QString DBAppExample::getMeasurementColor(QString measurementName)
{
    return mColorTable[measurementName].name();
}

void DBAppExample::createColorForMeasurement(QString measurementName)
{
    TTPLOG_FUNCTIONSCOPE;

    if (mColorTable.contains(measurementName)==false ) {

        quint16 ch = qChecksum(measurementName.toUtf8().data(),
                               measurementName.toUtf8().length());
        int s = (int)ch % 4;

        if ( s == 0 ) {
           mColorTable[measurementName] = QColor(Qt::blue);
        }
        else if ( s == 1 ) {
           mColorTable[measurementName] = QColor(Qt::red);
        }
        else if ( s == 2 ) {
           mColorTable[measurementName] = QColor(Qt::green);
        }
        else if ( s == 3 ) {
           mColorTable[measurementName] = QColor(Qt::magenta);
        }
        else if ( s == 4 ) {
           mColorTable[measurementName] = QColor(Qt::darkYellow);
        }
        else if ( s == 5 ) {
           mColorTable[measurementName] = QColor(Qt::darkGreen);
        }
        else {
            mColorTable[measurementName] = QColor(Qt::gray);
        }
    }
}
