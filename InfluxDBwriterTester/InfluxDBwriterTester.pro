# When setting up projects, do not use shadow building since examples will reference
# "$$PWD/../InfluxDBWriter/release/" and "$$PWD/../InfluxDBWriter/debug/" directories. 
# Examples tested in Windows 10 & Linux Ubuntu.

QT += core
QT -= gui

CONFIG += c++11

TARGET = InfluxDBwriterTester
CONFIG += console network core
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += src/main.cpp \
    src/tester.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../InfluxDBWriter/release/ -lInfluxDBWriter
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../InfluxDBWriter/debug/ -lInfluxDBWriter

INCLUDEPATH += $$PWD/../InfluxDBWriter/src
DEPENDPATH += $$PWD/../InfluxDBWriter/src

HEADERS += \
    src/tester.h
