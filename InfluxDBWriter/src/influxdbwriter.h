/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef INFLUXDBWRITER_H
#define INFLUXDBWRITER_H

#include <QMultiHash>
#include <QList>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <QSet>
#include <QMap>
#include "influxdbwriter_global.h"

/*! API for writing data to InfluxDB. Current implementation supports
 http access and no autentication */
class INFLUXDBWRITERSHARED_EXPORT InfluxDBWriter : public QObject
{
Q_OBJECT

public:
    InfluxDBWriter(QObject *parent = 0);
    ~InfluxDBWriter();

    /*! Connects to Influx database and start sending heartbeat.
     * Once heartbeat (ping) reply is received, connectedToDB signal will
     * be generated. */
    bool connectTo(QString connstring);

    /*! Clears Influx database (drop). */
    bool clearDB(QString dbName);

    /*! Creates db and takes it into use. */
    bool createAndUseDB(QString dbName);

    /*! Creates query for deleting measurement from DB */
    bool removeMeasurement(QString measurementName);

    /*! Write measurement to txqueue. Will be sent soon. */
    struct measureTag {
        QString tagName;
        QString tagValue;
    };

    /*! Writes measure into time series data base. Only one value field is supported */
    bool writeMeasure(QString measureName, qreal measureValue,
                      QList<InfluxDBWriter::measureTag> tags = QList<measureTag>(),
                      QDateTime timestamp = QDateTime::currentDateTime());

    /*! Writes measure into time series data base but adds coordinate information too for measure.
        This will mean that measurement will have additional fields. */
    bool writeLocationMeasure(QString measureName,qreal x,qreal y, qreal z, qreal measureValue,
                      QList<InfluxDBWriter::measureTag> tags = QList<measureTag>(),
                      QDateTime timestamp = QDateTime::currentDateTime());

    /*! Send query to database about measure names. Response signal
     *  queryMeasureNamesResponse will be qenerated when response
     *  is available.
     */
    bool queryMeasureNames();

    /*! Send query to database about available tag names. Response signal
     *  queryMeasureNamesResponse will be qenerated when response
     *  is available.
     */
    bool queryAllTags();
    bool queryAllTagValues(QString tagName);

    /*! Send query to database for querying data. FilterTags are using 'AND' manner */

    struct queryDataPointTag {
        QDateTime timeStamp; // timestamp
        QHash<QString,qreal> valueFields; // values
        QSet<QString> tagList; // tags
    };

    struct queryResultTag {
        bool partialResult; // if true, more data is coming from network
        bool queryWasSuccessful; // query result
        QString measureName; // measurename
        QMultiMap<QDateTime,queryDataPointTag> results; // datapoints
    };

    bool queryMeasureData(QString measureName, QList<measureTag> filterTags,
                          QDateTime startTime, QDateTime finishTime);

    /*! Get current operations pending (request sent to db but not responded yet */
    quint16 getPendingOperationCount();

signals:

    /*! Connection to db established */
    void connectedToDB();

    /*! Database clear operation result. */
    void databaseCleared(bool wasSuccess);

    /*! Database create operation result. */
    void databaseCreatedAndTakenIntoUse(bool operationOk);

    /*! No heartbeat (ping) received (timeout). */
    void disconnectedFromDB();

    /*! Influx DB replied that some data have been written. */
    void dataWritten();

    /*! Influx DB replied that measurement have been dropped Measurementname variable not yet in use. */
    bool removedMeasurement(bool wasSuccess,QString measurementName);

    /*! Response for measure names query */
    void queryMeasureNamesResponse(bool wasSuccess, QStringList measureNames);

    /*! Response for measure tags query. QMultihash groups  */
    void queryAllTagsResponse(bool wasSuccess,  QHash<QString,QSet<QString> >  tagNames);
    
    /*! Respone for querying single tags values */
    void queryTagValuesResponse(bool wasSuccess, QHash<QString, QHash<QString, QList<QString> > > tagValues);

    /*! Response for measure data query. Can be partial result  */
    void queryDataResponse(InfluxDBWriter::queryResultTag result);

private slots:
    void onFinished(QNetworkReply *reply);
    void onTimeOut();
    void onTxTimeout();

private:

    /* Influx DB 'SELECT *' returns both fields and tags. Following is used for filtering
     * unwanted (tags) from measurement data query result */
    const char *measureFieldPrefix= "#vf#";

    struct queryValueField {
        QString fieldName;
        qreal fieldValue;
    };

    struct request {
        QString operationName;
        QNetworkRequest *httpReguest;
        QNetworkReply *httpReply;
    };

    struct messageQueueItem {
        QString measureName;
        QList<queryValueField> measureValue;
        QDateTime timestamp;
        QList<measureTag> tags;
    };

    bool mConnectionToDatabaseOk;
    bool mWaitingPingReply;
    QTimer mHeartbeat;
    QTimer mTxflushTimer;
    QString mDBname;
    QString  mConnString;

    QNetworkAccessManager mManager;
    QHash<QNetworkReply  *,request> mPendingReplies;
    QList<messageQueueItem> mLineProtocolMessageQueue;

    void abortReguests();

    QString calculateTagSetAndMeasureName(const messageQueueItem &item);
    void cleanUpRequest(request r, QNetworkReply *reply);

    QStringList parseMeasurementsJSON(          QString response);
    QHash<QString,QSet<QString> > parseTagJSON( QString response);
    queryResultTag parseQueryDataJSON(          QString response);
    QHash<QString, QHash<QString, QList<QString> > >
    parseTagValuesJSON(                         QString response);

    void onRequestComplete(request r);
    void onRequestFailed(request r, QNetworkReply::NetworkError error);

    void sendPing();
    void sendTxQueue();
    void startTxTimer();

    // OperationName is reference that is used when looking up response. This should
    // be checked when receiving response or error. Check functions
    // onRequestComplete and onRequestFailed
    bool useGetRequest( QUrl &reqAddr, QUrlQuery  query,   QString operationName);
    bool usePostRequest(QUrl &reqAddr, QByteArray &payload,
                        QString operationName,
                        QString contentheaderType="application/x-www-form-urlencoded");

};

#endif // INFLUXDBWRITER_H
