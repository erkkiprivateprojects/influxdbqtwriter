# README #

Influx database API written on Qt
- write data
- read data
- query measurement names
- query tags


### What is this repository for? ###

This repo contains Qt API for reading&writing data from/to Influx timeserie database over http connection. 

### How do I get set up? ###

You need Influx database running (see https://www.influxdata.com/) that accepts http requests and creating of new databases. 
You need Qt 5.9 (tested on MinWg & Windows 10). Influx version was 1.2.2.

There are 2 test applications

1) InfluxDBappExample

GUI example of recording coordinates to database and on the fly heat map calculation. Please note that heatmap is not cached. Idea is to demo database API.

Filter buttons example for filtering view based on measurement names. There is also option to query tags but they are not used in this demo. 


2) InfluxDBwriterTester

Console example of recording data to database and querying them. 

### Before compiling ###

When setting up projects, do not use shadow building since examples will reference "$$PWD/../InfluxDBWriter/release/" and "$$PWD/../InfluxDBWriter/debug/" directories. 
Examples tested in Windows 10. 

### Instructions for InfluxDBwriterTester ###

Change database connectionString (src/tester.h) from example to suit your setup. Then compile both InfluxDBwriterTester and library. Run example. Console application connects to database. You should see "ready for logging" and soon after that continous updates. 

### Instructions for InfluxDBappExample: ###

Change database connectionString (src/app/dbappexample.h) from example to suit your setup. Then compile both InfluxDBappExample and library. Run example. When application starts it tries to connect database (gray screen where in center text "connecting <ip>" is displayed). Once connection is established you should see map of Oslo city center. Follow the instructions for demo.  

Logging and examining app:
When checking code you will something like "TTPLOG_FUNCTIONSCOPE;". Those are macros for tool called TimeToPic
logger. Load tools (TimeToPic bundle) from http://timetopic.herwoodtechnologies.com/products-services . Then you can measure timing visuallu such as http://timetopic.herwoodtechnologies.com/screenshots.
If you want disable those macros just remove "DEFINES+=TTPLOGGING" from .pro files. 

See screenshot (1/2) of app 
![appScreenshot](InfluxDBappExampleScreenshot.png)

See screenshot (2/2) of measurement setup
![measurementScreenshot](InfluxDBappMeasureScreenshot.png)


