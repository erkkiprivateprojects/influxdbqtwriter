/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef DBAPPEXAMPLE_H
#define DBAPPEXAMPLE_H

#include <QObject>
#include <QGeoCoordinate>
#include <QTimer>
#include <QColor>
#include <QElapsedTimer>
#include <QVariantList>
#include <QSet>

#include "influxdbwriter.h"
#include "heatmap.h"

class DBAppExample : public QObject
{
    Q_OBJECT
public:

    /* Change connection string suit your needs */
    const QString connectionString = "http://192.168.79.132:8086";

    explicit DBAppExample(QObject *parent = nullptr);
    ~DBAppExample();

    // this is for items that needs data from
    // DBAppExample
    static DBAppExample * instance();


    void registerHeatMap(QString heatMapName, HeatMap *instanceRef );
    void unRegisterHeatMap(QString heatMapName);

signals:
    void newPathPoint(QGeoCoordinate pathPoint, QString colorName);

    void connectingToDataBase(QString connString);

    void connectedToDatabase();
    void disconnectedFromDataBase();

    void measureNamesUpdated(QVariantList measureNames,QVariantList measureColorNames);

public slots:

    void start();

    void onAddNewPosition(QGeoCoordinate newPosition);
    void onClearData();

    void onDisplayUpdateRequest();

    /*! Starts new measurement serie */
    void onStartNewSerie();


    void addMeasureFilterItem(QString itemToBeFilteredOut);
    void removeMeasureFilterItem(QString itemToBeRemovedOut);
    void clearMeasureFilter();

private slots:
    void onConnectedToDB();
    void onDisconnectedFromDB();

    void onDatabaseCleared(bool wasSuccess);
    void onDatabaseCreatedAndTakenIntoUse(bool operationOk);
    void onDataWritten();

    void onQueryMeasureNamesResponse(bool wasSuccess,
                                     QStringList measureNames);

    void onQueryAllTagsResponse(bool wasSuccess,
                                QHash<QString,QSet<QString> > tagNames);

    void onQueryTagValuesResponse(bool wasSuccess,
                                  QHash<QString,
                                  QHash<QString, QList<QString> > > tagValues);

    void onQueryDataResponse(InfluxDBWriter::queryResultTag result);


    void onDataRefreshTimeOut();

private:

    void updateNextMeasurementName();

    QString getCurrentMeasurementName();
    QString getMeasurementColor(QString measurementName);

    void createColorForMeasurement(QString measurementName);

    InfluxDBWriter *mdbWriter;
    QString mDatabaseName;

    QString mCurrentMeasureName;
    quint8 mCurrentMeasureIndex;

    QElapsedTimer mLastUpdate;
    QTimer mDataRefreshTimer;

    QHash <QString, QColor> mColorTable;

    QSet<QString> mMeasureFilters;

    QHash<QString, HeatMap *> mHeatMaps;
};

#endif // DBAPPEXAMPLE_H
