/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import QtQuick 2.8
import QtQuick.Window 2.2
import QtPositioning 5.8
import com.herwoodtech.InfluxExamples 1.0
import QtLocation 5.3

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Influx DB App example")

    Component.onCompleted: {
        showMaximized()        
    }

    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...
        // specify plugin parameters if necessary
        // PluginParameter {
        //     name:
        //     value:
        // }
    }

    Map {
        id: myMap
        anchors.fill: parent
        plugin: mapPlugin
        center: QtPositioning.coordinate(59.91, 10.75) // Oslo
        zoomLevel: 16

        gesture.enabled: true
        gesture.acceptedGestures: MapGestureArea.PanGesture

        function clearMap() {
            myMap.clearMapItems()
        }

        function addPath(pathPos,itemColor) {
            var circle = Qt.createQmlObject('import QtLocation 5.3; MapCircle {}', myMap)
            circle.center = pathPos
            circle.radius = 6
            circle.color = itemColor
            circle.border.width = 0

            myMap.addMapItem(circle)
        }

        onCenterChanged: {
            myHeats.onVisibleAreaChanged(myMap.visibleRegion)
            myHeats.update()
            app.onDisplayUpdateRequest()
        }

        onZoomLevelChanged: {
            myHeats.onVisibleAreaChanged(myMap.visibleRegion)
            myHeats.update()
            app.onDisplayUpdateRequest()
        }

        Component.onCompleted: {

        }

    }

    HeatMap {
        id: myHeats
        anchors.fill: parent
    }

    DBAppExample {
        id: app

        onNewPathPoint: {
            myMap.addPath(pathPoint,colorName)
        }

        onMeasureNamesUpdated: {
            for (var i=0; i<measureNames.length; i++) {
                var measureName = measureNames[i]
                //http://doc.qt.io/qt-5/qml-qtqml-models-listmodel.html
                if (filterModel.containsMeasure(measureName)===false) {
                    filterModel.append({"measureName": measureName, "selected":true, "itemColor":measureColorNames[i]})
                }
            }
        }

        onConnectedToDatabase: {
            infoBox.infoText=""

            myHeats.onVisibleAreaChanged(myMap.visibleRegion)
            myHeats.update()
            app.onDisplayUpdateRequest()
        }

        onConnectingToDataBase: {
            infoBox.infoText="Connecting to "+connString
        }

        Component.onCompleted: {
            myHeats.registerHeatMap("exampleMap")
        }
    }

    Text {
        id: hintText
        text: qsTr("Press mouse button and move mouse (3+ secs). Then release mouse button.")
        font.pixelSize: myMap.height*1/32
        opacity: 0.5

        anchors.horizontalCenter: myMap.horizontalCenter
        anchors.verticalCenter: myMap.verticalCenter

    }

    MouseArea {
        anchors.fill: parent

        property bool recordingOn: false



        hoverEnabled: true

        onPressed: {
            recordingOn=true
            filterModel.enableAll()
            hintText.visible=false
        }

        onReleased: {
            recordingOn=false
            app.onDisplayUpdateRequest();
            hintText.visible=true
        }

        onRecordingOnChanged: {
            if ( recordingOn===true) {
                myMap.clearMap()
                app.onStartNewSerie()
            }
        }

        function addPos() {
            var loc
            loc = myMap.toCoordinate(Qt.point(mouseX,mouseY))
            app.onAddNewPosition(loc )
        }

        onMouseXChanged: {
            if (recordingOn===true ) {
                addPos()
            }
        }

        onMouseYChanged: {
            if (recordingOn===true ) {
                addPos()
            }
        }

        onClicked: {
        }
    }

    ListModel {
        id: filterModel

        function enableAll () {
            for (var k=0;k<filterModel.count;k++) {
               get(k).selected=true
               app.removeMeasureFilterItem( get(k).measureName)
            }
        }

        function containsMeasure(measureName) {
            var ret=false
            for (var k=0;k<filterModel.count;k++) {
                if ( get(k).measureName===measureName) {
                    ret = true
                    break
                }
            }
            return ret
        }
    }

    ListView {
        id: filters

        width: 100
        height: parent.height
        anchors.right:parent.right

        model: filterModel

        delegate: Rectangle {
            width: filters.width
            height: filters.height*(1/16)
            color: model.selected===true?model.itemColor:"gray"

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: model.measureName
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onClicked: {
                    if (model.selected===true ) {
                        model.selected=false
                        app.addMeasureFilterItem(model.measureName)
                        myMap.clearMap()
                        app.onDisplayUpdateRequest();
                    }
                    else {
                        model.selected=true
                        app.removeMeasureFilterItem(model.measureName)
                        myMap.clearMap()                        
                        app.onDisplayUpdateRequest();                        
                    }
                }
            }
        }
    }

    ModalInfo {
        id: infoBox
        anchors.fill: parent
    }
}
