# When setting up projects, do not use shadow building since examples will reference
# "$$PWD/../InfluxDBWriter/release/" and "$$PWD/../InfluxDBWriter/debug/" directories. 
# Examples tested in Windows 10 & Linux Ubuntu. 

TEMPLATE = app

QT += qml quick positioning
CONFIG += c++11

SOURCES += src/app/main.cpp \
    src/app/dbappexample.cpp \
    src/app/heatmap.cpp \
    src/timetopiclogger/timetopiclogger.cpp

RESOURCES += src/ui/qml.qrc

#Enable timetopic logger
DEFINES+=TTPLOGGING

win32:LIBS += -lpsapi

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = src/ui

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH = src/ui

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../InfluxDBWriter/release/ -lInfluxDBWriter
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../InfluxDBWriter/debug/ -lInfluxDBWriter

INCLUDEPATH += $$PWD/../InfluxDBWriter/src
DEPENDPATH += $$PWD/../InfluxDBWriter/src

HEADERS += \
    src/app/dbappexample.h \
    src/app/heatmap.h \
    src/timetopiclogger/timetopiclogger.h

INCLUDEPATH += src/timetopiclogger
