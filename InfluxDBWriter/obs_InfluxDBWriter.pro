# When setting up projects, do not use shadow building since examples will reference
# "$$PWD/../InfluxDBWriter/release/" and "$$PWD/../InfluxDBWriter/debug/" directories. 
# Examples tested in Windows 10 & Linux Ubuntu.

QT       += network

QT       -= gui

TARGET = InfluxDBWriter
TEMPLATE = lib

DEFINES += INFLUXDBWRITER_LIBRARY

SOURCES += src/influxdbwriter.cpp

HEADERS += src/influxdbwriter.h\
        src/influxdbwriter_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
