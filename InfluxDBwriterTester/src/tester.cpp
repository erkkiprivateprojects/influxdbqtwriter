/*
Copyright 2017 Erkki Salonen (erkki@herwoodtechnologies.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <QtGlobal>
#include <QDateTime>

#include "tester.h"

Tester::Tester(QObject *parent) : QObject(parent) , mdbWriter(NULL),
    mDatabaseName("timetopicLogger"), mTestMeasureCount(0)
{
    connect(&mQueryTimer, &QTimer::timeout,
            this, &Tester::onQueryTestTimerTimeOut);
}

Tester::~Tester()
{
    mdbWriter->deleteLater();
    mdbWriter=NULL;
}

void Tester::start()
{
    mdbWriter = new InfluxDBWriter();

    connect(mdbWriter, &InfluxDBWriter::connectedToDB,
            this, &Tester::onConnectedToDB);

    connect(mdbWriter, &InfluxDBWriter::disconnectedFromDB,
            this, &Tester::onDisconnectedFromDB);

    connect(mdbWriter, &InfluxDBWriter::databaseCleared,
            this, &Tester::onDatabaseCleared);

    connect(mdbWriter, &InfluxDBWriter::databaseCreatedAndTakenIntoUse,
            this, &Tester::onDatabaseCreatedAndTakenIntoUse);

    connect(mdbWriter, &InfluxDBWriter::dataWritten,
            this, &Tester::onDataWritten);

    connect(mdbWriter, &InfluxDBWriter::queryAllTagsResponse,
            this, &Tester::onQueryAllTagsResponse);

    connect(mdbWriter, &InfluxDBWriter::queryMeasureNamesResponse,
            this, &Tester::onQueryMeasureNamesResponse);

    connect(mdbWriter, &InfluxDBWriter::queryDataResponse,
            this, &Tester::onQueryDataResponse);

    connect(mdbWriter, &InfluxDBWriter::queryTagValuesResponse,
            this, &Tester::onQueryTagValuesResponse);

    // your influx db address    
    mdbWriter->connectTo(connectionString);
}

void Tester::onConnectedToDB()
{
    mdbWriter->clearDB(mDatabaseName);

    const int queryTestIntervalMs=5000;
    mQueryTimer.start(queryTestIntervalMs);
}

void Tester::onDisconnectedFromDB()
{
    mQueryTimer.stop();
}

void Tester::onDatabaseCleared(bool wasSuccess)
{
    if (wasSuccess==true) {
        /* Writing to DB can now start */
        if (mdbWriter->createAndUseDB(mDatabaseName)==true) {
        }
    }
}

void Tester::onDatabaseCreatedAndTakenIntoUse(bool operationOk)
{
    if ( operationOk==true) {
        /* Writing to DB can now start */
        qDebug() << "ready for logging";

        // create tags if needed
        QList<InfluxDBWriter::measureTag> tags;
        InfluxDBWriter::measureTag tag;
        tag.tagName =  QString("car");
        tag.tagValue =   QString("%1").arg(0);
        tags.append(tag);

        tag.tagName =  QString("engine");
        tag.tagValue =   QString("%1").arg(0);
        tags.append(tag);

        QString measureName= "rpm";
        mdbWriter->writeMeasure(measureName,mTestMeasureCount,tags);
    }
}

void Tester::onDataWritten()
{
    mTestMeasureCount++;
    if ( mTestMeasureCount<10000) {

        int rCarNumber= qrand() % 10;
        int rCarEngineNumber= qrand() % 4;

        QList<InfluxDBWriter::measureTag> tags;

        // create tags if needed
        InfluxDBWriter::measureTag tag;
        tag.tagName =  QString("car");
        tag.tagValue =   QString("%1").arg(rCarNumber);
        tags.append(tag);

        tag.tagName =  QString("engine");
        tag.tagValue =   QString("%1").arg(rCarEngineNumber);
        tags.append(tag);

        QString measureName= "mileage";
        mdbWriter->writeMeasure(measureName,mTestMeasureCount,tags);

        measureName= "mileage without tags";
        tags.clear();
        mdbWriter->writeMeasure(measureName,mTestMeasureCount,tags);

        // test location measure API
        QList<InfluxDBWriter::measureTag> locationTags;
        InfluxDBWriter::measureTag locTag1;
        locTag1.tagName="area";
        locTag1.tagValue="south1";
        locationTags.append(locTag1);

        mdbWriter->writeLocationMeasure("positionInfo",1,2,3,10,locationTags);
        //qDebug() << "Write measure" << mTestMeasureCount;
    }
    else {
        qDebug() << "Wrote " << mTestMeasureCount << "datapoints. Test complete";
    }
}

void Tester::onQueryTestTimerTimeOut()
{
    mdbWriter->queryAllTags();
    mdbWriter->queryMeasureNames();
}

void Tester::onQueryMeasureNamesResponse(bool wasSuccess, QStringList measureNames)
{
    if ( wasSuccess == true ) {

        qDebug() << __FUNCTION__ << "measure names available " <<  measureNames;

        for (QString measure : measureNames ) {

            QList<InfluxDBWriter::measureTag> sample;
#if 0
            /* Add limiting filter using tag */
            InfluxDBWriter::measureTag m1;
            m1.tagName="car";
            m1.tagValue="1";
            sample.append(m1);
#endif
            mdbWriter->queryMeasureData(measure,
                                 sample,
                                 QDateTime::currentDateTime().addSecs(-10),
                                 QDateTime::currentDateTime());
        }
    }
}

void Tester::onQueryAllTagsResponse(bool wasSuccess, QHash<QString,QSet<QString> > tagNames)
{
    if ( wasSuccess == true ) {
        qDebug() << __FUNCTION__ << "tag names available " <<  tagNames;

        /* Query each tag values */
        for (QString measureName : tagNames.keys()) {
            for ( QString tagName : tagNames.value(measureName).values() ) {
                mdbWriter->queryAllTagValues(tagName);
            }
        }
    }
}

void Tester::onQueryTagValuesResponse(bool wasSuccess, QHash<QString, QHash<QString, QList<QString> > > tagValues)
{
    if ( wasSuccess == true ) {
        qDebug() << __FUNCTION__ << "tag values available " <<  tagValues;
    }
}

void Tester::onQueryDataResponse(InfluxDBWriter::queryResultTag result)
{
    if ( result.queryWasSuccessful==true) {
        //if (result.partialResult==true) {
        if ( 1 ) {
            /* collect */
            qDebug() << __FUNCTION__ << "data available " <<  result.results.size() << " datapoints";

            for (InfluxDBWriter::queryDataPointTag v : result.results) {

                qDebug() << v.timeStamp.toString();
                QHash<QString,qreal>::const_iterator i = v.valueFields.constBegin();
                while (i != v.valueFields.constEnd()) {
                    qDebug() << i.key() << ": " << i.value();
                    ++i;
                }
            }
        }
        else {
            /* last */
            qDebug() << __FUNCTION__ << "data available " <<  result.results.size() << " datapoints";
        }
    }
}
