# When setting up projects, do not use shadow building since examples will reference "$$PWD/../InfluxDBWriter/release/"
# and "$$PWD/../InfluxDBWriter/debug/" directories.

TEMPLATE = subdirs

SUBDIRS += \
    InfluxDBWriter \
    InfluxDBwriterTester \
    InfluxDBappExample
